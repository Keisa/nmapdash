const	navtab_ini = () => {
	const	navtab_btns = $('.navtab > nav button');

	navtab_btns.click(function () {
		let	target;

		target = this.getAttribute('data-target');
		if (target === undefined)
			return ;
		navtab_btns.removeClass('active');
		this.classList.add('active');
		$('.navtab > .tabs > .tab').removeClass('active');
		$(target).addClass('active');
	});
};

$(document).ready(function() {
	navtab_ini();

	/*
		item, scaninfo, verbose, debugging, host, runstats
	*/

	$.get('/ajax/localhost', function (data) {
		try {
			$('#data').html(data);
			data = JSON.parse(data);
			data.forEach(function (item) {
				console.log(item);
				console.log(item.hostname);
				item.hosts.forEach(function (host) {
					console.table(host.hostnames[0])
					console.table(host.addresses);
				});
				console.log('---------');
			});
		}
		catch (e) {
			console.log(e);
		}
	});
});
