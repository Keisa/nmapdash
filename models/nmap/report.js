'use strict';

const	fs = require('fs');
const nmap = require('libnmap');

function Report(_hosts, _flags) {
	var		hosts = _hosts;
	var		flags = _flags;
	const	$this = this;
	
	this.getHosts = function() {
		return (hosts);
	}

	this.scan = function(callback) {
		let	opts = {
			flags: flags,
			range: hosts
		};
		nmap.scan(opts, function(err, report) {
			let	obj;
			let	elm;
			let	data;

			if (err)
				throw err;
			for (let item in report) {
				obj = report[item];
				fs.writeFile('./tmp/' + item, JSON.stringify(obj), function () {
					callback($this, item);
				});
			}
		});
	}

	this.load = function (parse, callback, end) {
		let	size;

		size = 0;
		hosts.forEach(function (host) {
			fs.readFile('./tmp/' + host, function (err, data) {
				if (err)
					console.log(err);
				callback($this, { hostname: host, data: data.toString() });
				size++;
				if (size == hosts.length)
					end();
			});
		});
	}

	this.display = function() {
		this.load(function (sender) {
			console.log($this.getHosts());
			items.forEach(function(host) {
				host.data.host.forEach(function(host) {
					host.ports.forEach(function(item) {
						item.port.forEach(function(item) {
							console.log('protocol: ' + item.item.protocol);
							console.log('portid: ' + item.item.portid);
							item.service.forEach(function(service) {
								console.log('service name: ' + service.item.name);
								if (service.item.product)
									console.log('product: ' + service.item.product);
								if (service.item.version)
									console.log('version: ' + service.item.version);
								if (service.item.ostype)
									console.log('ostype: ' + service.item.ostype);
							});
							console.log('_______');
						});
					});
				});
				console.log('--------------------------------');
			});
		});
	}
}

module.exports = Report;
