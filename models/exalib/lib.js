'use strict';
const	lib = {
	isFunction: function (obj) {
		return (typeof obj === 'function');
	}
};

module.exports = lib;
