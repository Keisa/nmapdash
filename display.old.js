	console.log('Starting Nmap ' + data.item.version + ' at ' + data.item.startstr);
	console.log('Nmap scan report for ' + obj.hostname);
	console.log('Host is ' + data.host[0].status[0].item.state);
	console.log('PORT\tSTATE\tSERVICE\tVERSION');

	ret.hostname = obj.hostname;
	ret.ports = [];
	ret.status = data.host[0].status[0].item.state;
	data.host[0].ports.forEach(function (item) {
		item.port.forEach(function (port) {
			let	out;
			let	serviceinfo = '';
			out = port.item.portid + '/' + port.item.protocol + '\t';
			port.state.forEach(function (state) {
				out += state.item.state;
			});
			out += '\t';
			port.service.forEach(function (service) {
				let	serv = {};

				serv.name = service.item.name;
				if (service.item.product)
					serv.product = service.item.product;
				if (service.item.version)
					serv.version = service.item.version;
				if (service.item.ostype)
					serv.ostype = service.item.ostype;
				out += service.item.name + '\t' + service.item.product + ' ' + service.item.version
				if (service.item.ostype)
					serviceinfo = 'Service info: OS: ' + service.item.ostype;
			});
			// console.log(out);
			// console.log(serviceinfo);
		});
		// ret.ports.push(portObj);
	});
	return ret;
