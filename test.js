'use strict';

let	argv;

if (process.argv.length == 2)
{
	console.log('hosts..');
	return (1);
}
argv = process.argv;
argv.splice(0, 2);
console.log(argv);
const nmap = require('libnmap');
const opts = {
  flags: [
    '-sV', // Open port to determine service (i.e. FTP, SSH etc)
    '-sC', // Enables the nmap scripts (all) against each host (requires elevated privileges)
  ],
  // range: [ 'scanme.nmap.org', '172.17.0.0/24' ]
  range: argv
};

nmap.scan(opts, (err, report) => {
	let	data;

	if (err)
		throw err;

	for (let item in report) {
		console.log(JSON.stringify(report[item], null, 2));
		continue ;
		console.log(item);
		data = report[item];
		if (data.host) {
			data.host.forEach((host) => {
				host.ports.forEach((item) => {	
					item.port.forEach((item) => {
						console.log(item.item.protocol + ', ' + item.item.portid);
						item.service.forEach((service) => {
							console.log(service.item.name);
							if (service.item.product)
								console.log(service.item.product);
							if (service.item.version)
								console.log(service.item.version);
							if (service.item.ostype)
								console.log(service.item.ostype);
						});
					});
				});
			});
		}
		console.log("-------------------------------------");
	}
});
