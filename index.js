'use strict';
const	express = require('express');
const	bodyParser = require('body-parser');
// const	mongoose = require('mongoose');
const	path = require('path');
const	hpp = require('hpp');
const	favicon  = require('serve-favicon');

const	lib = require('./models/exalib/lib');
const	Report = require('./models/nmap/report.js');

const	app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(hpp());

app.use(favicon(path.join('public/images', 'favicon.ico')));

/* set the absolute path to serve static files */
app.use('/', express.static((__dirname, 'public')));

/* set ejs module config */
app.set('view engine', 'ejs');

/* change the default views directory to 'pages' */
app.set('views', path.join(__dirname, 'pages'));

app.get('/', function (req, res) {
	res.render('index');
});

function format(obj) {
	let	data;
	let	ret = {
		hostname: '',
		hosts: [{
			status: '',
			addresses: [ { addr: '', addrtype: '' } ],
			hostnames: [ { hostname: [ { name: '', type: '' } ] } ],
			ports: [{
				id: 'portid', protocol: 'tcp/udp/...', state: 'open|close|filtred',
				service: [{ name: '', product: 'if exist', version: 'if exist', ostype: 'if exist', cpe: 'if exists' }]
			}]
		}]
	};
	ret.hosts = []
	ret.hostname = obj.hostname;
	data = JSON.parse(obj.data);
	data.host.forEach(function (host) {
		let	hostObj = {};

		hostObj.addresses = [];
		hostObj.hostnames = [];
		hostObj.ports = [];
		if (host.status.length > 0 && host.status[0].item)
			hostObj.status = host.status[0].item.state;
		host.address.forEach(function (addr) {
				hostObj.addresses.push({ addr: addr.item.addr, addrtype: addr.item.addrtype });
		});
		hostObj.hostnames = host.hostnames;
		if (host.ports.length > 0)
		{
			host.ports[0].port.forEach(function (port) {
				let	portObj = {};

				portObj.id = port.item.portid;
				portObj.protocol = port.item.protocol;
				portObj.state = port.state[0].item.state;
				hostObj.ports.push(portObj);
			});
		}
		ret.hosts.push(hostObj);
	});
	return (ret);
}

app.get('/ajax/:hosts', function (req, res) {
	let	hosts;
	let	report;

	if (req.params.hosts === undefined)
		return ;
	hosts = req.params.hosts.split(',');
	res.writeHead(200, { 'Content-Type': 'text/plain' });
	report = new Report(hosts, [ '-sV', '-sC' ]);
	let	array = [];
	report.load(false, function (sender, obj) {
		array.push(obj);
	}, function () {
		let	arr = [];
		// res.write(JSON.stringify(array));
		array.forEach(function (val) {
			arr.push(format(val));
		});
		res.write(JSON.stringify(arr));
		res.end();
	});
	/*report.scan(function (sender) {
		let	array = [];
		sender.load(false, function (sender, obj) {
			array.push(obj);
		}, function () {
			res.write(JSON.stringify(array));
			res.end();
		});
	});*/
});

const	PORT = 1338;

app.listen(PORT, function () {
	console.log(`server start listening at port ${PORT}`);
});
